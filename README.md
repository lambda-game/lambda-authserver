# Lambda auth server
Multiplayer auth server for lambda

### Flow

1. Launcher logs user in, and stores auth token
2. Launcher tells game the auth token
3. Game tells multiplayer server auth token when logging in
4. Multiplayer server checks auth token, gets back username.
5. Multiplayer server sets player username to the returned username, and auth token to the players auth token
