package com.interfiber.lambda.authserver.db;

import com.interfiber.lambda.authserver.AuthserverApplication;
import com.interfiber.lambda.authserver.SqlUtils;
import com.interfiber.lambda.authserver.db.user.Password;
import com.interfiber.lambda.authserver.db.user.User;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

public class AuthServerPG {
    static Connection connection;
    static Logger logger = LoggerFactory.getLogger(AuthserverApplication.class);

    public static void execute(String stmt) {
        try {
            Statement st = connection.createStatement();
            boolean rs = st.execute(stmt);
            st.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> executeQuery(String queryString){
        try {
            Statement st = connection.createStatement();
            ResultSet set = st.executeQuery(queryString);
            ArrayList<String> result = new ArrayList<>();
            while (set.next()){
                result.add(set.getString(1));
            }
            set.close();
            st.close();
            return result;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static void connectToDatabase(){
        try {
            PGSimpleDataSource ds = new PGSimpleDataSource();
            ds.setApplicationName("lambdaAuthServer");
            ds.setSslMode("require");
            ds.setUser("authroot");
            ds.setPassword(Optional.ofNullable(System.getenv("AUTHSERVER_DB_PASSWORD")).orElseThrow(() -> new IllegalArgumentException("AUTHSERVER_DB_PASSWORD not set")));
            ds.setUrl(Optional.ofNullable(System.getenv("AUTHSERVER_DB_URL")).orElseThrow(
                    () -> new IllegalArgumentException("AUTHSERVER_DB_URL is not set.")));
           connection = ds.getConnection();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void createDefaults(){
        String[] statements = {"CREATE TABLE IF NOT EXISTS users (id UUID PRIMARY KEY DEFAULT gen_random_uuid(), username STRING, passwordHash STRING, accountCreateDate STRING, authToken STRING);"};

        execute(statements[0]);
    }

    public static CreateUserStatus createUser(String username, String password){

        if (!SqlUtils.isSqlInjectionSafe(username)){
            logger.error("not allowing username, matches sqlCheckPattern");
            return CreateUserStatus.DISALLOWED_USERNAME;
        } else if (!SqlUtils.isSqlInjectionSafe(username)){
            logger.error("not allowing password, matches sqlCheckPattern");
            return CreateUserStatus.DISALLOWED_PASSWORD;
        }

        if (userExists(username)){
            logger.error("user already exists");
            return CreateUserStatus.TAKEN_USERNAME;
        }

        if (!username.matches("[a-zA-Z]+")){
           return CreateUserStatus.DISALLOWED_USERNAME;
        }

        String hashPassword = Password.hashPassword(password);
        execute("INSERT INTO users (username, passwordHash, accountCreateDate, authToken) VALUES ('" + username + "', '" + hashPassword + "', '" + Instant.now().getEpochSecond() + "', '" + UUID.randomUUID() + "');");
        return CreateUserStatus.CREATED_USER;
    }

    public static boolean userExists(String username){
        if (!SqlUtils.isSqlInjectionSafe(username)){
            logger.error("userExists(...) input not sql injection safe");
            return true;
        }
        ArrayList<String> result = executeQuery("select * from users where username='" + username + "'");

        if (result == null){
            logger.error("query failed, assuming a user exists");
            return true;
        }

        return result.size() != 0;

    }

    public static User getUser(String username, String password){

        if (!SqlUtils.isSqlInjectionSafe(username) || !SqlUtils.isSqlInjectionSafe(password)){
            logger.error("getUser(..., ...) input is not sql safe");
            return null;
        }

        if (!userExists(username)){
            return null;
        }

        User foundUser = new User();
        boolean didFind = false;

        try {
            Statement st = connection.createStatement();
            ResultSet set = st.executeQuery("select * from users where username='" + username + "'");

            while (set.next()){
                boolean isSame = Password.checkPassword(password, set.getString("passwordHash"));
                if (isSame) {
                    foundUser.authToken = set.getString("authToken");
                    foundUser.username = set.getString("username");
                    foundUser.createDate = set.getString("accountCreateDate");
                    foundUser.password = password;
                    didFind = true;
                    break;
                }
            }
            set.close();
            st.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        if (!didFind){
            logger.error("user query failed");
            return null;
        } else {
            return foundUser;
        }
    }

    public static User getUserByToken(String token){

        if (!SqlUtils.isSqlInjectionSafe(token)){
            logger.error("getUserByToken(...) input is not sql safe");
            return null;
        }

        User foundUser = new User();
        boolean didFind = false;

        try {
            Statement st = connection.createStatement();
            ResultSet set = st.executeQuery("select * from users where authToken='" + token + "'");

            if (set.next()) {
                foundUser.authToken = set.getString("authToken");
                foundUser.username = set.getString("username");
                foundUser.createDate = set.getString("accountCreateDate");
                didFind = true;
            }

            set.close();
            st.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        if (!didFind){
            logger.error("token query failed");
            return null;
        } else {
            return foundUser;
        }
    }
}
