package com.interfiber.lambda.authserver.db.user;

import at.favre.lib.crypto.bcrypt.BCrypt;

public class Password {
    public static String hashPassword(String password){
        return BCrypt.withDefaults().hashToString(12, password.toCharArray());
    }

    public static boolean checkPassword(String passwordRaw, String passwordHashed){
        return BCrypt.verifyer().verify(passwordRaw.toCharArray(), passwordHashed.toCharArray()).verified;
    }
}
