package com.interfiber.lambda.authserver.db;

public enum CreateUserStatus {
    DISALLOWED_PASSWORD,
    DISALLOWED_USERNAME,
    TAKEN_USERNAME,
    CREATED_USER,
    RATELIMITED
}
