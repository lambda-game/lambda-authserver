package com.interfiber.lambda.authserver;

import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;

@Component
public class Customizer implements WebServerFactoryCustomizer<ConfigurableWebServerFactory> {
    @Override
    public void customize(ConfigurableWebServerFactory factory) {
        // Setting the port number
        int port = 8080;
        if (System.getenv("PORT") != null){
            port = Integer.parseInt(System.getenv("PORT"));
        }
        factory.setPort(port);
    }
}