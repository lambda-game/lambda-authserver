package com.interfiber.lambda.authserver.responses;

import com.interfiber.lambda.authserver.db.user.User;

public class AuthenticateUserResponse {
    public User user;
    public String message;
}
