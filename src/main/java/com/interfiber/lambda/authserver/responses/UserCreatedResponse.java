package com.interfiber.lambda.authserver.responses;

import com.interfiber.lambda.authserver.db.CreateUserStatus;

public class UserCreatedResponse {
    public CreateUserStatus status;
    public String message;
}
