package com.interfiber.lambda.authserver;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.local.LocalBucket;

import java.time.Duration;

public class RateLimiting {
    public static Bucket bucket;

    public static void create(){
        Bandwidth limit = Bandwidth.simple(2, Duration.ofMinutes(4));
        bucket = Bucket.builder().addLimit(limit).build();
    }
}
