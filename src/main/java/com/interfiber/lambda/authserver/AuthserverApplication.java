package com.interfiber.lambda.authserver;

import com.interfiber.lambda.authserver.db.AuthServerPG;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthserverApplication {

	public static void main(String[] args) {
		AuthServerPG.connectToDatabase();
		AuthServerPG.createDefaults();
		RateLimiting.create();

		SpringApplication.run(AuthserverApplication.class, args);
	}

}
