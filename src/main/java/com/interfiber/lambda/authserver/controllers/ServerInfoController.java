package com.interfiber.lambda.authserver.controllers;

import com.interfiber.lambda.authserver.RateLimiting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServerInfoController {
   @GetMapping("/server")
    public String serverInfo(){
       return "LambdaAuthServer(Spring) version v1.0";
   }
}
