package com.interfiber.lambda.authserver.controllers;

import com.interfiber.lambda.authserver.AuthserverApplication;
import com.interfiber.lambda.authserver.RateLimiting;
import com.interfiber.lambda.authserver.db.AuthServerPG;
import com.interfiber.lambda.authserver.db.CreateUserStatus;
import com.interfiber.lambda.authserver.db.user.User;
import com.interfiber.lambda.authserver.requests.AuthenticateFromTokenRequest;
import com.interfiber.lambda.authserver.requests.RegisterUserRequest;
import com.interfiber.lambda.authserver.responses.AuthenticateUserResponse;
import com.interfiber.lambda.authserver.responses.UserCreatedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserManagementController {
    Logger logger = LoggerFactory.getLogger(AuthserverApplication.class);

    // register a new user, with a username and password
    @PostMapping("/api/v1/auth/users/register")
    public UserCreatedResponse register(@RequestBody RegisterUserRequest body){
        if (!RateLimiting.bucket.tryConsume(1)){
            UserCreatedResponse response = new UserCreatedResponse();
            response.status = CreateUserStatus.RATELIMITED;
            response.message = "Too many requests";
            return response;
        }
        logger.info("Registering new user");
        CreateUserStatus status = AuthServerPG.createUser(body.username, body.password);
        UserCreatedResponse response = new UserCreatedResponse();
        response.status = status;
        if (response.status != CreateUserStatus.CREATED_USER) {
            response.message = "User registration failed";
        } else {
            response.message = "User registration complete";
        }

        return response;
    }


    // returns the authToken for the provided user. Takes in a username and password for the user
    @PostMapping("/api/v1/auth/users/authenticateFromCreds")
    public AuthenticateUserResponse getUser(@RequestBody RegisterUserRequest body){
        User user = AuthServerPG.getUser(body.username, body.password);
        AuthenticateUserResponse response = new AuthenticateUserResponse();
        if (user == null){
            response.message = "A user with that username, or password does not exist";
        } else {
            response.message = "A user was found in the database";
            response.user = user;
        }
        return response;
    }

    // returns the user that owns an auth token
    @PostMapping("/api/v1/auth/users/authenticateFromToken")
    public AuthenticateUserResponse getUserFromToken(@RequestBody AuthenticateFromTokenRequest body){
        User user = AuthServerPG.getUserByToken(body.token);
        AuthenticateUserResponse response = new AuthenticateUserResponse();
        if (user == null){
            response.message = "Invalid token";
        } else {
            response.message = "Valid token";
            response.user = user;
        }
        return response;
    }
}
